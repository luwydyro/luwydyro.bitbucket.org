
$(document).ready(function() {

  /**Efecto del logo */
  function addclase(){
    var logo_i = $('#luwy-brand-img');

    if (logo_i.hasClass('active')) {
        $(logo_i).removeClass('active');
        $(logo_i).removeClass('flip');
        $(logo_i).addClass('inactive');
    }
    else {
        $(logo_i).removeClass('inactive');
        $(logo_i).addClass('active');
        $(logo_i).addClass('flip');
    }
  }
  setInterval(addclase, 4000);



  /** Efecto boton visitar ahora */
  var btnlink = $("#btnnow");
  $(btnlink).click(function(){
    $("#ldfrase").addClass("animated bounceOutDown");
    $("#ldtitle").removeClass("animated zoomIn");
    $("#ldtitle").addClass("animated bounceOutUp");
    $("#btnnow").addClass("animated bounceOutDown");
    setTimeout(function(){
      $(".luwy-brand").addClass("ldzoomin");
    }, 500);
  })

  // Mobile Navigation
  if( $('#nav-menu-container').length ) {
      var $mobile_nav = $('#nav-menu-container').clone().prop({ id: 'mobile-nav'});
      $mobile_nav.find('> ul').attr({ 'class' : '', 'id' : '' });
      $('body').append( $mobile_nav );
      $('body').prepend( '<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>' );
      $('body').append( '<div id="mobile-body-overly"></div>' );
      $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

      $(document).on('click', '.menu-has-children i', function(e){
          $(this).next().toggleClass('menu-item-active');
          $(this).nextAll('ul').eq(0).slideToggle();
          $(this).toggleClass("fa-chevron-up fa-chevron-down");
      });

      $(document).on('click', '#mobile-nav-toggle', function(e){
          $('body').toggleClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').toggle();
      });

      $(document).click(function (e) {
          var container = $("#mobile-nav, #mobile-nav-toggle");
          if (!container.is(e.target) && container.has(e.target).length === 0) {
             if ( $('body').hasClass('mobile-nav-active') ) {
                  $('body').removeClass('mobile-nav-active');
                  $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
                  $('#mobile-body-overly').fadeOut();
              }
          }
      });
  } else if ( $("#mobile-nav, #mobile-nav-toggle").length ) {
      $("#mobile-nav, #mobile-nav-toggle").hide();
  }


  $('.scrolltop, #logo a').click(function(){
    $("html, body").animate({
      scrollTop: 0
    }, 1000);
    return false;
  });



  $('.ancla2').click(function(){
    var link = $(this);
    var anchor  = link.attr('href');
    $('html, body').stop().animate({
      scrollTop: jQuery(anchor).offset().top
    }, 3000);



    if ( $(this).parents('.nav-menu').length ) {
      $('.nav-menu .menu-active').removeClass('menu-active');
      $(this).closest('li').addClass('menu-active');
    }

    if ( $('body').hasClass('mobile-nav-active') ) {
        $('body').removeClass('mobile-nav-active');
        $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
        $('#mobile-body-overly').fadeOut();
    }

    setTimeout(function(){
      $("#ldfrase").removeClass("bounceOutDown");
      $("#ldfrase").addClass("bounce");
      $("#ldtitle").removeClass("animated bounceOutUp");
      $("#ldtitle").addClass("animated bounce");
      $("#btnnow").removeClass("animated bounceOutDown");
      $("#btnnow").addClass("animated bounce");
      $(".luwy-brand").removeClass("ldzoomin");
      $(".luwy-brand").addClass("ldzoomout");
    }, 6000);
    return false;
  });



  /** Efecto del Ancla del menu */
  $('.ancla').click(function(){
    var link = $(this);
    var anchor  = link.attr('href');
    $('html, body').stop().animate({
      scrollTop: jQuery(anchor).offset().top
    }, 1000);
    if ( $(this).parents('.nav-menu').length ) {
      $('.nav-menu .menu-active').removeClass('menu-active');
      $(this).closest('li').addClass('menu-active');
    }

    if ( $('body').hasClass('mobile-nav-active') ) {
        $('body').removeClass('mobile-nav-active');
        $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
        $('#mobile-body-overly').fadeOut();
    }
    return false;
  });

});





document.addEventListener('DOMContentLoaded', function() {
  var typed = new Typed('#typed', {
    stringsElement: '#typed-strings',
    typeSpeed: 30,
    backSpeed: 30,
    startDelay: 1000,
    loop: true,
    loopCount: Infinity,
    onComplete: function(self) { prettyLog('onComplete ' + self) },
    preStringTyped: function(pos, self) { prettyLog('preStringTyped ' + pos + ' ' + self); },
    onStringTyped: function(pos, self) { prettyLog('onStringTyped ' + pos + ' ' + self) },
    onLastStringBackspaced: function(self) { prettyLog('onLastStringBackspaced ' + self) },
    onTypingPaused: function(pos, self) { prettyLog('onTypingPaused ' + pos + ' ' + self) },
    onTypingResumed: function(pos, self) { prettyLog('onTypingResumed ' + pos + ' ' + self) },
    onReset: function(self) { prettyLog('onReset ' + self) },
    onStop: function(pos, self) { prettyLog('onStop ' + pos + ' ' + self) },
    onStart: function(pos, self) { prettyLog('onStart ' + pos + ' ' + self) },
    onDestroy: function(self) { prettyLog('onDestroy ' + self) }
  });


});

function prettyLog(str) {
  //console.log('%c ' + str, 'color: green; font-weight: bold;');
}

function toggleLoop(typed) {
  if (typed.loop) {
    typed.loop = false;
  } else {
    typed.loop = true;
  }
}
