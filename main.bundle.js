webpackJsonp([2],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n<app-header></app-header>\n<app-portafolio></app-portafolio>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__ = __webpack_require__("../../../../../src/app/services/informacion.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(_is) {
        this._is = _is;
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_informacion_service__ = __webpack_require__("../../../../../src/app/services/informacion.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_header_header_component__ = __webpack_require__("../../../../../src/app/components/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_footer_footer_component__ = __webpack_require__("../../../../../src/app/components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_portafolio_portafolio_component__ = __webpack_require__("../../../../../src/app/components/portafolio/portafolio.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



/***Servicios */

/**Componentes*/




var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_5__components_header_header_component__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_6__components_footer_footer_component__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components_portafolio_portafolio_component__["a" /* PortafolioComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* HttpModule */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_3__services_informacion_service__["a" /* InformacionService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"contact\" id=\"contacto\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-12 text-center\">\n        <h2 class=\"pb-5\">Contacto</h2>\n      </div>\n    </div>\n\n    <div class=\"row\">\n\n      <div class=\"col-lg-6\">\n        <div class=\"form\">\n          <div id=\"sendmessage\">Mensaje Enviado</div>\n          <div id=\"errormessage\"></div>\n          <form action=\"\" method=\"post\" role=\"form\" class=\"contactForm\">\n            <div class=\"row\">\n              <div class=\"col-lg-6\">\n                <div class=\"form-group\">\n                  <input type=\"text\" name=\"name\" class=\"form-control\" id=\"name\" placeholder=\"Nombre\" data-rule=\"minlen:4\" data-msg=\"Minimo 4 digitos\"\n                  />\n                  <div class=\"validation\"></div>\n                </div>\n              </div>\n              <div class=\"col-lg-6\">\n                <div class=\"form-group\">\n                  <input type=\"email\" class=\"form-control\" name=\"email\" id=\"email\" placeholder=\"Email\" data-rule=\"email\" data-msg=\"Ingrese su mail válido\"\n                  />\n                  <div class=\"validation\"></div>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <textarea class=\"form-control py-4\" name=\"message\" rows=\"5\" data-rule=\"required\" data-msg=\"Ingrese su mensaje\" placeholder=\"Ingrese su Mensaje\"></textarea>\n              <div class=\"validation\"></div>\n            </div>\n\n            <button class=\"btn btn-primary btn-md\" type=\"submit\">Enviar Información</button>\n          </form>\n        </div>\n      </div>\n\n      <div class=\"col-lg-5 ml-auto\">\n        <div class=\"info pl-4 pt-5  \">\n          <div>\n\n            <p>\n              <i class=\"fa fa-map-marker\"></i>\n              {{_is.info.direccion}}\n            </p>\n          </div>\n\n          <div>\n            <p>\n              <i class=\"fa fa-envelope-o\" aria-hidden=\"true\"></i>\n              {{_is.info.email}}\n            </p>\n          </div>\n\n          <div>\n\n            <p>\n              <i class=\"fa fa-mobile\"></i>\n              {{_is.info.telefono}}\n            </p>\n          </div>\n\n        </div>\n      </div>\n\n    </div>\n  </div>\n</section>\n\n<footer class=\"site-footer\">\n  <div class=\"bottom\">\n    <div class=\"container\">\n      <div class=\"row\">\n\n        <div class=\"col-lg-6 col-xs-12 text-lg-left text-center\">\n          <p class=\"copyright-text\">\n              {{_is.info.nombre_corto}} © {{anio}}\n          </p>\n          <div class=\"credits\">\n\n\n          </div>\n        </div>\n\n        <div class=\"col-lg-6 col-xs-12 text-lg-right text-center\">\n          <p class=\"copyright-text\">\n            <a [href]=\"_is.info.pagina_autor\">luwydyro.com</a>\n             - Derechos Reservados\n          </p>\n        </div>\n\n      </div>\n    </div>\n  </div>\n</footer>\n"

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__ = __webpack_require__("../../../../../src/app/services/informacion.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FooterComponent = (function () {
    function FooterComponent(_is) {
        this._is = _is;
        this.anio = new Date().getFullYear();
        this.anio;
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    return FooterComponent;
}());
FooterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-footer',
        template: __webpack_require__("../../../../../src/app/components/footer/footer.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */]) === "function" && _a || Object])
], FooterComponent);

var _a;
//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"luwy\">\n    <div class=\"container text-center\">\n      <div class=\"row\">\n        <div class=\"col-md-12\">\n          <a class=\"luwy-brand\">\n            <img class=\"animated infinite flip active \" id=\"luwy-brand-img\" alt=\"Luwy Dyro - Desarrollador Web\" src=\"./assets/img/logo.svg\">\n          </a>\n        </div>\n      </div>\n\n      <div class=\"col-md-12\">\n        <h1 id=\"ldtitle\" class=\"animated zoomIn\"> LUWY DYRO </h1>\n\n        <div class=\"type-wrap tagline\" id=\"ldfrase\">\n          <div id=\"typed-strings\">\n            <span>Desarrollador <strong>Web</strong> </span>\n            <p>Maquetador </p>\n            <p>Diseñador </p>\n            <p>UX/UI </p>\n          </div>\n          <span id=\"typed\" style=\"white-space:pre;\"></span>  Front End Web\n        </div>\n\n        <a id=\"btnnow\" class=\"btn btn-outline-primary btn-md px-5 py-3 ancla2\" href=\"#acercademi\" >Visitar Ahora </a>\n      </div>\n    </div>\n  </section>\n\n<!-- /luwy -->\n\n<header class=\"sticky-top d-flex align-items-center\">\n  <div class=\"container\">\n    <div class=\"row justify-content-between align-items-center\">\n      <div id=\"logo\">\n        <a href=\"index.html\">\n          <img src=\"./assets/img/logo-web.svg\" alt=\"Luwy Dyro Desarrollador FrontEnd Web\">\n        </a>\n      </div>\n\n      <nav class=\"d-flex ml-auto mr-auto\" id=\"nav-menu-container\">\n        <ul class=\"nav-menu\">\n          <li>\n            <a class=\"ancla\" href=\"#acercademi\">Acerca de mi</a>\n          </li>\n          <li>\n            <a class=\"ancla\" href=\"#experiencias\">Experiencias</a>\n          </li>\n          <li>\n            <a class=\"ancla\" href=\"#habilidades\">Habilidades</a>\n          </li>\n          <li>\n            <a class=\"ancla\" href=\"#portfolio\">Portfolio</a>\n          </li>\n\n          <li>\n            <a class=\"ancla\" href=\"#contacto\">Contacto</a>\n          </li>\n        </ul>\n      </nav>\n      <!-- #nav-menu-container -->\n\n      <nav class=\"nav social-nav d-lg-block d-none\">\n        <a href=\"{{_is.info.twitter}}\" target=\"_blank\">\n          <i class=\"fa fa-twitter\"></i>\n        </a>\n        <!-- <a href=\"#\">\n          <i class=\"fa fa-facebook\"></i>\n        </a> -->\n        <a href=\"{{_is.info.linkedin}}\">\n          <i class=\"fa fa-linkedin\"></i>\n        </a>\n        <a href=\"mailto:luwy.dyro@gmail.com\">\n          <i class=\"fa fa-envelope\"></i>\n        </a>\n      </nav>\n    </div>\n  </div>\n\n</header>\n"

/***/ }),

/***/ "../../../../../src/app/components/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__ = __webpack_require__("../../../../../src/app/services/informacion.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = (function () {
    function HeaderComponent(_is) {
        this._is = _is;
    }
    return HeaderComponent;
}());
HeaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-header',
        template: __webpack_require__("../../../../../src/app/components/header/header.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */]) === "function" && _a || Object])
], HeaderComponent);

var _a;
//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/portafolio/portafolio.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"about\" id=\"acercademi\">\n  <div class=\"container text-center\">\n    <h2>\n      Acerca de mi\n    </h2>\n    <p>\n      Mi nombre es Luis Huaman Valverde conocido hace mucho tiempo como\n      <span>\"Luwy\"</span>, les doy la bienvenida a mi sitio web donde les detallo mi hoja de vida profesional. Soy un profesional\n      titulado en “Computación e Informática”, mi interés por el desarrollo de soluciones y creatividades vistas en el mundo   del internet me llevo a especializarme en el diseño UX/UI y desarrollo de sitios Web.\n    </p>\n    <p>\n      Empece diseñando gráficas para páginas web, UX/UI,  maquetando y ahora en el campo del frontend, Mi objetivo es tener el conocimiento amplio en el diseño y desarrolloweb cumpliendo\n      requerimientos de manera eficiente y sostenible en el tiempo utilizando diferentes plataformas.\n    </p>\n\n\n  </div>\n</section>\n\n\n\n\n<div class=\"parallax-window\" data-parallax=\"scroll\" data-image-src=\"./assets/img/bg-welcome.jpg\">\n  <div class=\"container \">\n    <div class=\"block row flex-column justify-content-center align-items-center text-center\">\n      <h2> Front End web </h2>\n      <p class=\"text-center\">\n        Profesional con conocimientos de diversas áreas que me permiten\n        <br> organizar, dirigir y ejecutar un proyecto.\n      </p>\n    </div>\n  </div>\n\n</div>\n\n\n<section id=\"experiencias\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center mb-5\">\n      <h2>Experiencias</h2>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-lg-12\">\n        <ul class=\"timeline\">\n          <li>\n            <div class=\"timeline-image\">\n              <img class=\"rounded-circle img-fluid\" src=\"./assets/img/exp-1.jpg\" alt=\"\">\n            </div>\n            <div class=\"timeline-panel\">\n              <div class=\"timeline-heading\">\n                <h4>2008-2011</h4>\n                <h4 class=\"subheading\">Macrobusiness Corp. SAC</h4>\n              </div>\n              <div class=\"timeline-body\">\n                <p class=\"text-muted\">Soporte de Maquinas, Bocetos de Páginas Web, Implemenatción en PHP y MYSQL, Webs realializasa en CMS Wordpress\n                  y Joomla. Diseños en Photoshop e Illustrator.</p>\n              </div>\n            </div>\n          </li>\n          <li class=\"timeline-inverted\">\n            <div class=\"timeline-image\">\n              <img class=\"rounded-circle img-fluid\" src=\"./assets/img/exp-2.jpg\" alt=\"\">\n            </div>\n            <div class=\"timeline-panel\">\n              <div class=\"timeline-heading\">\n                <h4>2011 - 2013 </h4>\n                <h4 class=\"subheading\">CGM Holding Business SAC </h4>\n              </div>\n              <div class=\"timeline-body\">\n                <p class=\"text-muted\">Encargado de administrador las cuentas de los clientes, mantenimiento y creación de Sitios Web. control del\n                  SEO y SEM, aplicaciones en PHP nativo. y MySql. Diseños en Photoshop e Illustrator.</p>\n              </div>\n            </div>\n          </li>\n          <li>\n            <div class=\"timeline-image\">\n              <img class=\"rounded-circle img-fluid\" src=\"./assets/img/exp-3.jpg\" alt=\"\">\n            </div>\n            <div class=\"timeline-panel\">\n              <div class=\"timeline-heading\">\n                <h4>2013 - 2015</h4>\n                <h4 class=\"subheading\">E-TEXTIL EIRL</h4>\n              </div>\n              <div class=\"timeline-body\">\n                <p class=\"text-muted\">Encargado de Diseñar los sitios web e Intranets de la empresa, maquetación responsive basados en HTML, Stylus,\n                  Jquery para ser pasados al Área de Programacion .NET. apoyo al area de programación con desarrollos de\n                  aplicaciones bàsicas en PHP y Mysql.</p>\n              </div>\n            </div>\n          </li>\n          <li class=\"timeline-inverted\">\n            <div class=\"timeline-image\">\n              <img class=\"rounded-circle img-fluid\" src=\"./assets/img/exp-4.jpg\" alt=\"\">\n            </div>\n            <div class=\"timeline-panel\">\n              <div class=\"timeline-heading\">\n                <h4>2015 - 2018</h4>\n                <h4 class=\"subheading\">SAMISHOP SAC</h4>\n              </div>\n              <div class=\"timeline-body\">\n                <p class=\"text-muted\">Propuestas y desarrollos webs para clientes de la empresa y para el CMS SAMISHOP E-Commerce, Mobile Fisrt, Maquetación responsive, Revisiones de calidad. Funcionalidades en Javascript, Jquery, UI, animations. IDE Visual Code, Visual Studio\n                  js. </p>\n              </div>\n            </div>\n          </li>\n          <li class=\"timeline-inverted\">\n            <div class=\"timeline-image\">\n              <h4>\n                Luwy\n                <br> Dyro\n              </h4>\n            </div>\n          </li>\n        </ul>\n      </div>\n    </div>\n  </div>\n</section>\n\n\n\n\n\n<section class=\"cta\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-lg-9 col-sm-12 text-lg-left text-center\">\n        <h2>\n          Developed Front End / Designed web\n        </h2>\n\n        <p>\n          Soy un diseñador y desarrolador web especializado en extructurar, codificar y mantener tantos sitios web como apliaciones web innovadoras y reutilizables.\n        </p>\n      </div>\n\n      <div class=\"col-lg-3 col-sm-12 text-lg-right text-center\">\n        <a class=\"btn btn-ghost px-5 py-2 ancla \" href=\"#contacto\">Contacto</a>\n      </div>\n    </div>\n  </div>\n</section>\n\n\n\n<section class=\"features\" id=\"habilidades\">\n  <div class=\"container\">\n    <h2 class=\"text-center pb-4\">\n      Habilidades\n    </h2>\n\n    <div class=\"row\">\n\n      <div class=\"feature-col col-lg-4 col-xs-12\">\n        <div class=\"card card-block text-center\">\n          <div>\n            <div class=\"feature-icon\">\n              <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>\n\n            </div>\n          </div>\n\n          <div>\n            <h3>\n              Diseño Web UX/UI\n            </h3>\n\n            <p>\n              Wireframes, Photoshop, Illustrator, Corel Draw,\n              <br> Multimedia, AE\n            </p>\n          </div>\n        </div>\n      </div>\n      <div class=\"feature-col col-lg-4 col-xs-12\">\n        <div class=\"card card-block text-center\">\n          <div>\n            <div class=\"feature-icon\">\n              <i class=\"fa fa-laptop\" aria-hidden=\"true\"></i>\n            </div>\n          </div>\n\n          <div>\n            <h3>\n              Frontend Web\n            </h3>\n\n            <p>\n              HTML 5, Bootstrap 4.0, SCSS - Stylus - Flexbox -Animation, Javascript - Jquery, Angular 4\n            </p>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"feature-col col-lg-4 col-xs-12\">\n        <div class=\"card card-block text-center\">\n          <div>\n            <div class=\"feature-icon\">\n              <i class=\"fa fa-code\" aria-hidden=\"true\"></i>\n            </div>\n          </div>\n\n          <div>\n            <h3>\n              Desarrollo - CMS\n            </h3>\n            <p>\n              PHP, Mysql, Firebase, Wordpress, Joomla,\n              <br> Magento, Blogs\n            </p>\n\n          </div>\n        </div>\n      </div>\n\n\n    </div>\n\n    <div class=\"row\">\n      <div class=\"feature-col col-lg-4 col-xs-12\">\n        <div class=\"card card-block text-center\">\n          <div>\n            <div class=\"feature-icon\">\n              <i class=\"fa fa-window-maximize\" aria-hidden=\"true\"></i>\n            </div>\n          </div>\n\n          <div>\n            <h3>\n              IDE's\n            </h3>\n\n            <p>\n              Visual Studio Code, Sublime Text 3, Atom\n            </p>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"feature-col col-lg-4 col-xs-12\">\n        <div class=\"card card-block text-center\">\n          <div>\n            <div class=\"feature-icon\">\n              <i class=\"fa fa-github\" aria-hidden=\"true\"></i>\n            </div>\n          </div>\n\n          <div>\n            <h3>\n              Gestor - Repos\n            </h3>\n\n            <p>\n              Git, GitHub, Gulp, Webpack,\n              <br>Mailchimp, Mail Relay.\n            </p>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"feature-col col-lg-4 col-xs-12\">\n        <div class=\"card card-block text-center\">\n          <div>\n            <div class=\"feature-icon\">\n              <i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>\n            </div>\n          </div>\n\n          <div>\n            <h3>\n              Tools\n            </h3>\n\n            <p>\n              Windows, Trello, Evernote, Codepen, hackerthemes, CSSDeck, Ofimática...\n            </p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n\n\n\n<section class=\"portfolio pb-5\" id=\"portfolio\">\n\n  <h2 class=\"text-center pb-4\">\n    Portfolio\n  </h2>\n\n\n  <div class=\"container\">\n    <div class=\"card-deck pb-4\">\n\n\n      <div *ngFor = \"let datos of _is.portafolio\"\n            class=\"card mb-4\">\n        <img class=\"card-img-top\" src=\"{{datos.img}}\" alt=\"Card image cap\">\n        <div class=\"card-body\">\n          <h4 class=\"card-title\">{{datos.titulo}}</h4>\n          <p class=\"card-text\">{{datos.contenido}}</p>\n        </div>\n        <div class=\"card-footer\">\n          <small class=\"text-muted\">\n            <a href=\"http://{{datos.url}}\" target=\"_blank\">{{datos.url}}</a>\n          </small>\n        </div>\n      </div>\n\n    </div>\n\n\n  </div>\n\n\n\n</section>\n\n\n\n<section class=\"social\" id=\"social\">\n  <div class=\"container\">\n    <h2 class=\"text-center mb-4\">\n      Sociales\n    </h2>\n\n    <div class=\"row justify-content-center\">\n      <a href=\"#\" class=\" mx-2\">\n        <i class=\" fa fa-twitter  p-4 border border-primary  rounded-circle\"></i>\n      </a>\n      <a href=\"#\" class=\" mx-2\">\n        <i class=\" fa fa-facebook p-4 border border-primary  rounded-circle\"></i>\n      </a>\n      <a href=\"#\" class=\" mx-2\">\n        <i class=\" fa fa-linkedin p-4 border border-primary  rounded-circle\"></i>\n      </a>\n      <a href=\"#\" class=\" mx-2\">\n        <i class=\" fa fa-envelope p-4 border border-primary  rounded-circle\"></i>\n      </a>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "../../../../../src/app/components/portafolio/portafolio.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__ = __webpack_require__("../../../../../src/app/services/informacion.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PortafolioComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PortafolioComponent = (function () {
    function PortafolioComponent(_is) {
        this._is = _is;
    }
    PortafolioComponent.prototype.ngOnInit = function () {
    };
    return PortafolioComponent;
}());
PortafolioComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-portafolio',
        template: __webpack_require__("../../../../../src/app/components/portafolio/portafolio.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_informacion_service__["a" /* InformacionService */]) === "function" && _a || Object])
], PortafolioComponent);

var _a;
//# sourceMappingURL=portafolio.component.js.map

/***/ }),

/***/ "../../../../../src/app/services/informacion.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformacionService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InformacionService = (function () {
    function InformacionService(http) {
        this.http = http;
        this.info = {};
        this.cargada = false;
        this.cargada_portafolio = false;
        this.portafolio = [];
        this.carga_info();
        this.carga_portafolio();
    }
    /**Carga Datos del Json local */
    InformacionService.prototype.carga_info = function () {
        var _this = this;
        this.http.get('assets/data/info.pagina.json')
            .subscribe(function (data) {
            //console.log(data.json());
            _this.cargada = true;
            _this.info = data.json();
        });
    };
    /**Carga dde datos del Firebase */
    InformacionService.prototype.carga_portafolio = function () {
        var _this = this;
        this.http.get('https://webluwy.firebaseio.com/portafolio.json')
            .subscribe(function (data) {
            //console.log(data.json());
            _this.cargada_portafolio = true;
            _this.portafolio = data.json();
        });
    };
    return InformacionService;
}());
InformacionService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], InformacionService);

var _a;
//# sourceMappingURL=informacion.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map